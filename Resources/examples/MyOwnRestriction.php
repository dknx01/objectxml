<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 25.09.16 15:08
 */

namespace Dknx01\ObjectXml\Resources\examples;

use Dknx01\ObjectXml\Exception\InvalidContentException;
use Dknx01\ObjectXml\Restriction\RestrictionInterface;

/**
 * example to show who to implement a custom restriction
 */
class MyOwnRestriction implements RestrictionInterface
{
    /**
     * @inheritDoc
     */
    public function validate($value)
    {
        if ($value != 42) {
            throw new InvalidContentException('The number must be the answer to the life, the universe and everything');
        }
    }
}