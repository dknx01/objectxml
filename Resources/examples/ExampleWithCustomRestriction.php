#! /usr/bin/php
<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 25.09.16 14:38
 */

namespace Dknx01\ObjectXml\Resources\examples;

require_once __DIR__ . '/../../vendor/autoload.php';

use Dknx01\ObjectXml\Element\StringElement;
use Dknx01\ObjectXml\Exception\InvalidContentException;
use Dknx01\ObjectXml\ObjectXml;
use Dknx01\ObjectXml\XmlGenerator;

/**
 * A simple example how to use the ObjectXml library
 */
class ExampleWithCustomRestriction
{
    public function createXml()
    {
        $empty = new StringElement(42, 'ToTheLife', null, new MyOwnRestriction());

        $objectXml = new ObjectXml();
        $objectXml->add($empty);

        $xmlGenerator = new XmlGenerator();

        echo $xmlGenerator->createXml('TheAnswer', $objectXml);

        echo PHP_EOL . 'The Exception if the value was wrong:' . PHP_EOL;
        try {
            $wrongAnswer = new StringElement(12, 'EmptyString', null, new MyOwnRestriction());
        } catch (InvalidContentException $exception) {
            echo 'File: ' . $exception->getFile() . PHP_EOL;
            echo 'Message: ' . $exception->getMessage() . PHP_EOL;
            echo 'ExceptionClass: ' . get_class($exception) . PHP_EOL;
        }
    }
}

$simpleClass = new ExampleWithCustomRestriction();
$simpleClass->createXml();