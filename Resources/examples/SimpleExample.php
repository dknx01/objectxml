#! /usr/bin/php
<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 25.09.16 14:38
 */

namespace Dknx01\ObjectXml\Resources\examples;

require_once __DIR__ . '/../../vendor/autoload.php';

use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Element\BaseAttribute;
use Dknx01\ObjectXml\Element\BaseElement;
use Dknx01\ObjectXml\Element\BooleanElement;
use Dknx01\ObjectXml\Element\CollectionElement;
use Dknx01\ObjectXml\Element\DateElement;
use Dknx01\ObjectXml\Element\DateTimeElement;
use Dknx01\ObjectXml\Element\FloatElement;
use Dknx01\ObjectXml\Element\IntegerElement;
use Dknx01\ObjectXml\Element\StringElement;
use Dknx01\ObjectXml\Element\TimeElement;
use Dknx01\ObjectXml\ObjectXml;
use Dknx01\ObjectXml\XmlGenerator;

/**
 * A simple example how to use the ObjectXml library
 */
class SimpleExample
{
    public function createXml()
    {
        $professionAttr = new BaseAttribute('profession', 'doctor');
        $ageAttr = new BaseAttribute('age', 'too young');
        $name = new StringElement('John F***', 'firstName', new AttributeCollection(array($professionAttr, $ageAttr)));

        $date = new DateElement('2016-01-01', 'FirstDay');

        $time = new TimeElement('12:00:00', 'Time');

        $dateTime = new DateTimeElement('2016-01-01 12:00:00', 'DateTime');

        $bool = new BooleanElement(true, 'bool');

        $float = new FloatElement(1.42, 'float');

        $integer = new IntegerElement(42, 'int');

        $integerAttributeCollection = new AttributeCollection();
        $integerAttributeCollection->add(new BaseAttribute('interessting', 'not really, but maybe'));
        $integer2 = new IntegerElement(55, 'aNumber', $integerAttributeCollection);
        $subCollection = new CollectionElement($integer2, 'SubCollection');
        $elementCollection = new CollectionElement($subCollection, 'Collection');

        $empty = new StringElement(null, 'EmptyString');

        echo 'All attributes for integer element: ';
        print_r($integerAttributeCollection->getAttributes());

        $objectXml = new ObjectXml();
        $objectXml->add($name)
            ->add($integer)
            ->add($date)
            ->add($time)
            ->add($dateTime)
            ->add($bool)
            ->add($float)
            ->add($integer)
            ->add($elementCollection)
            ->add($empty)
        ;

        $xmlGenerator = new XmlGenerator();

        echo $xmlGenerator->createXml('foo', $objectXml);
    }
}

$simpleClass = new SimpleExample();
$simpleClass->createXml();