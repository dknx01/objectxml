<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 16.09.16 20:17
 */

namespace Dknx01\ObjectXml\Restriction;

use Dknx01\ObjectXml\Exception\InvalidContentException;

/**
 * Restriction for checking if the value is a date time matching the given date format (default: Y-m-d H:i:s)
 */
class DateTimeRestriction implements RestrictionInterface
{
    const BASE_PATTERN = '/^\d+-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])\ '.
        '(0[0-9]|1[0-9]|2[0-3]):(0[0-9]|[1-5][0-9]):(0[0-9]|[1-5][0-9])$/';

    /**
     * @var string
     */
    private $pattern;

    /**
     * @param string $pattern
     */
    public function __construct($pattern = DateTimeRestriction::BASE_PATTERN)
    {
        $this->pattern = $pattern;
    }

    /**
     * @inheritDoc
     */
    public function validate($value)
    {
        if (!(bool)preg_match($this->pattern, $value)) {
            throw new InvalidContentException($value . ' does not match the date time pattern ' . $this->pattern);
        }
    }
}