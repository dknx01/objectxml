<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 25.09.16 14:34
 */

namespace Dknx01\ObjectXml\Restriction;

use Dknx01\ObjectXml\Exception\InvalidContentException;

/**
 * Restriction for checking that the value is only one of the allowed values
 */
class RestrictedValuesRestriction implements RestrictionInterface
{
    /**
     * @var array
     */
    private $allowedValues;

    /**
     * @param array $allowedValues
     */
    public function __construct(array $allowedValues)
    {
        $this->allowedValues = $allowedValues;
    }

    /**
     * @inheritDoc
     */
    public function validate($value)
    {
        if (!in_array($value, $this->allowedValues, true)) {
            throw new InvalidContentException(
                'The value  "' . $value . '" is not in the allowed values: ' . implode(', ', $this->allowedValues)
            );
        }
    }
}