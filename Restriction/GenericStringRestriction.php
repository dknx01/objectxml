<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 03.10.16 21:38
 */

namespace Dknx01\ObjectXml\Restriction;

use Dknx01\ObjectXml\Exception\InvalidContentException;

/**
 * Restriction for all valid generic string values.
 * Valid are:
 * <ul>
 * <li> strings </li>
 * <li> numbers (int, float) </li>
 * <li> boolean </li>
 * <li> null </li>
 * </ul>
 */
class GenericStringRestriction implements RestrictionInterface
{
    /**
     * @inheritDoc
     */
    public function validate($value)
    {
        if (is_object($value)
            || is_array($value)
            || is_resource($value)
        ) {
            throw new InvalidContentException('The value is not valid generic string.');
        }
    }
}