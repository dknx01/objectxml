<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 12.09.16 20:49
 */

namespace Dknx01\ObjectXml\Restriction;

use Dknx01\ObjectXml\Exception\InvalidContentException;

/**
 * Interface for all restrictions
 */
interface RestrictionInterface
{
    /**
     * This method contains the logic for the restriction, which decides if the given value is valid or not.
     *
     * @param mixed $value
     *
     * @throws InvalidContentException
     */
    public function validate($value);
}