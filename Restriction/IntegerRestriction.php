<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 16.09.16 20:33
 */

namespace Dknx01\ObjectXml\Restriction;

use Dknx01\ObjectXml\Exception\InvalidContentException;

/**
 * Restriction for checking if the value is an integer value
 */
class IntegerRestriction implements RestrictionInterface
{
    /**
     * @inheritDoc
     */
    public function validate($value)
    {
        if (!is_int($value)) {
            throw new InvalidContentException('Value must be a integer, ' . gettype($value) . ' given');
        }
    }
}