<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 21:11
 */

namespace Dknx01\ObjectXml\Restriction;
use Dknx01\ObjectXml\Exception\InvalidContentException;

/**
 * Restriction for checking if the value is matching the given regex pattern
 */
class RegexRestriction implements RestrictionInterface
{
    /**
     * @var string
     */
    private $pattern;

    /**
     * RegexRestriction constructor.
     * @param string $pattern
     */
    public function __construct($pattern)
    {
        $this->pattern = $pattern;
    }

    /**
     * @inheritDoc
     */
    public function validate($value)
    {
        if (!(bool)preg_match($this->pattern, $value)) {
            throw new InvalidContentException($value . ' does not match the pattern ' . $this->pattern);
        }
    }
}