<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 16.09.16 18:18
 */

namespace Dknx01\ObjectXml\Restriction;

use Dknx01\ObjectXml\Exception\InvalidContentException;

/**
 * Restriction for checking if the value is a date matching the given date format (default: Y-m-d)
 */
class DateRestriction implements RestrictionInterface
{
    /**
     * @var string
     */
    private $pattern;

    /**
     * @param string $pattern
     */
    public function __construct($pattern = '/^\d+-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/')
    {
        $this->pattern = $pattern;
    }

    /**
     * @inheritDoc
     */
    public function validate($value)
    {
        if (!(bool)preg_match($this->pattern, $value)) {
            throw new InvalidContentException($value . ' does not match the date pattern ' . $this->pattern);
        }
    }
}