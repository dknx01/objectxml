<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 16.09.16 20:30
 */

namespace Dknx01\ObjectXml\Restriction;

use Dknx01\ObjectXml\Exception\InvalidContentException;

/**
 * Restriction for checking if the value is a float value
 */
class FloatRestriction implements RestrictionInterface
{
    /**
     * @inheritDoc
     */
    public function validate($value)
    {
        if (!is_float($value)) {
            throw new InvalidContentException('Value must be a float, ' . gettype($value) . ' given');
        }
    }
}