<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 16.09.16 19:20
 */

namespace Dknx01\ObjectXml\Restriction;

use Dknx01\ObjectXml\Exception\InvalidContentException;

/**
 * Restriction for checking if the value is a time matching the given time format (default: H:i:s)
 */
class TimeRestriction implements RestrictionInterface
{
    /**
     * @var string
     */
    private $pattern;

    /**
     * DateRestriction constructor.
     * @param string $pattern
     */
    public function __construct($pattern = '/^(0[0-9]|1[0-9]|2[0-3]):(0[0-9]|[1-5][0-9]):(0[0-9]|[1-5][0-9])$/')
    {
        $this->pattern = $pattern;
    }

    /**
     * @inheritDoc
     */
    public function validate($value)
    {
        if (!(bool)preg_match($this->pattern, $value)) {
            throw new InvalidContentException($value . ' does not match the time pattern ' . $this->pattern);
        }
    }
}