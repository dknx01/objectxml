<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 12.09.16 21:06
 */

namespace Dknx01\ObjectXml\Restriction;
use Dknx01\ObjectXml\Exception\InvalidContentException;

/**
 * Restriction for checking if the value is boolean (true, false, 0 or 1)
 */
class BooleanRestriction implements RestrictionInterface
{
    /**
     * @var array
     */
    private $allowedvalues = array(0, 1, false, true);

    /**
     * @inheritDoc
     */
    public function validate($value)
    {
        if (is_object($value) || !in_array($value, $this->allowedvalues, true)) {
            throw new InvalidContentException(
                is_object($value) ? 'Object ' : $value . ' must be an boolean value (0, 1, true or false )'
            );
        }
    }
}