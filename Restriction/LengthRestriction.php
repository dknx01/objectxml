<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 25.09.16 14:31
 */

namespace Dknx01\ObjectXml\Restriction;

use Dknx01\ObjectXml\Exception\InvalidContentException;

/**
 * Restriction for checking the length (min and/or max) of a given value
 */
class LengthRestriction extends RangeRestriction
{
    /**
     * @inheritDoc
     */
    public function validate($value)
    {
        if (!is_string($value)) {
            throw new InvalidContentException('value must be a string');
        }

        if ($this->min !== null && (strlen($value) < $this->min)) {
            throw  new InvalidContentException(
                'The text cannot be empty or shorter than ' . $this->min
            );
        }

        if ($this->max !== null && (strlen($value) > $this->max)) {
            throw  new InvalidContentException(
                'The text cannot be longer than ' . $this->max
            );
        }
    }

}