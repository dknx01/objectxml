<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 12.09.16 21:08
 */

namespace Dknx01\ObjectXml\Restriction;
use Dknx01\ObjectXml\Exception\InvalidContentException;

/**
 * Restriction for checking if the value is within the given range (integer or float value)
 */
class RangeRestriction implements RestrictionInterface
{
    /**
     * @var int|float
     */
    protected $min;

    /**
     * @var int|float
     */
    protected $max;

    /**
     * @param float|int $min
     * @param float|int $max
     */
    public function __construct($min, $max)
    {
        $this->min = $min;
        $this->max = $max;
    }

    /**
     * @inheritDoc
     */
    public function validate($value)
    {
        if (!is_int($value) && !is_float($value)) {
            throw new InvalidContentException($value . ' must be of type integer or float');
        }
        if (!($value >= $this->min && $value <= $this->max)) {
            throw new InvalidContentException($value . ' must be within range of ' . $this->min . ' and ' . $this->max);
        }
    }
}