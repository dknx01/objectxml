<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 03.10.16 21:32
 */

namespace Dknx01\ObjectXml\Exception;

/**
 * Exception for an invalid attribute
 */
class InvalidAttributeException extends \Exception
{
}