<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 21:27
 */

namespace Dknx01\ObjectXml\Exception;

/**
 * Exception for invalid arguments added to elements, attributes or collections
 */
class InvalidArgumentException extends \Exception
{
}