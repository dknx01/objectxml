<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 22:11
 */

namespace Dknx01\ObjectXml\Exception;

/**
 * An exception tho inform that a second attribute with the same name was trying to added to an attribute collection
 */
class DoubleAttributeException extends \Exception
{
}