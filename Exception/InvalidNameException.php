<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 21:37
 */

namespace Dknx01\ObjectXml\Exception;

/**
 * exception for invalid name
 */
class InvalidNameException extends \Exception
{
}