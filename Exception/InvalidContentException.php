<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 21:02
 */

namespace Dknx01\ObjectXml\Exception;

/**
 * exception for invalid content for elements
 */
class InvalidContentException extends \Exception
{
}