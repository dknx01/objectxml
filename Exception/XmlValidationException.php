<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 25.09.16 12:24
 */

namespace Dknx01\ObjectXml\Exception;

/**
 * exception for an error when the xml was validated against an xsd file
 */
class XmlValidationException extends \Exception
{
}