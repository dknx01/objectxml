<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 20:49
 */

namespace Dknx01\ObjectXml\Exception;

/**
 * exception for an invalid xml tag name
 */
class InvalidTagNameException extends \Exception
{
}