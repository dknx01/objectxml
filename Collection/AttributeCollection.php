<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 12.09.16 20:33
 */

namespace Dknx01\ObjectXml\Collection;

use Dknx01\ObjectXml\Element\AttributeInterface;
use Dknx01\ObjectXml\Element\AttributesAwareInterface;
use Dknx01\ObjectXml\Element\BaseAttribute;
use Dknx01\ObjectXml\Exception\DoubleAttributeException;
use Dknx01\ObjectXml\Exception\InvalidArgumentException;

/**
 * A collection of multiple attributes
 */
class AttributeCollection implements CollectionInterface, AttributesAwareInterface
{
    /**
     * @var array
     */
    private $elements = array();

    /**
     * @param AttributeInterface[] $elements
     */
    public function __construct(array $elements = array())
    {
        foreach ($elements as $element) {
            if (!is_null($element)) {
                $this->add($element);
            }
        }
    }

    /**
     * @inheritDoc
     *
     * @throws InvalidArgumentException
     */
    public function add($element)
    {
        $this->checkAttributeInterfaceImplementation($element);
        $this->existsAttribute($element);
        $this->elements[] = $element;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function clear()
    {
        $this->elements = array();
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isEmpty()
    {
        return empty($this->elements);
    }

    /**
     * @inheritDoc
     */
    public function remove($name)
    {
        /**
         * @var int $key
         * @var BaseAttribute $value
         */
        foreach ($this->elements as $key => $value) {
            if ($value->getName() === $name) {
                unset($this->elements[$key]);
            }
        }
        return $this;
    }

    /**
     * Checks whether the collection contains an element with the specified key/index.
     *
     * @param string|integer $key The key/index to check for.
     *
     * @return boolean TRUE if the collection contains an element with the specified key/index,
     *                 FALSE otherwise.
     */
    public function containsKey($key)
    {
        return $this->exists(function ($element) use ($key) {
            /** @var BaseAttribute $element */
            return $element->getName() === $key;
        });
    }

    /**
     * @inheritDoc
     */
    public function get($key)
    {
        return $this->containsKey($key) ? current(array_filter($this->elements, function ($element) use ($key) {
            /** @var AttributeInterface $element */
            if ($element->getName() === $key) {
                return $element;
            }
        })) : null;
    }

    /**
     * @inheritDoc
     *
     * @throws DoubleAttributeException
     */
    public function set($key, $value)
    {
        if ($this->containsKey($key)) {
            throw new DoubleAttributeException(
                'An attribute wit the name ' . $key . ' already exists for this element'
            );
        }
        $this->elements[] = new BaseAttribute($key, $value);
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function exists(\Closure $p)
    {
        foreach ($this->elements as $element) {
            if ($p($element)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    public function filter(\Closure $p)
    {
        return $this->createFrom(array_filter($this->elements, $p));
    }

    /**
     * @inheritDoc
     */
    public function map(\Closure $func)
    {
        return $this->createFrom(array_map($func, $this->elements));
    }

    /**
     * @inheritDoc
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->elements);
    }

    /**
     * @inheritDoc
     */
    public function count()
    {
        return count($this->elements);
    }

    /**
     * @inheritDoc
     *
     * @throws DoubleAttributeException
     */
    public function addAttribute(AttributeInterface $attribute)
    {
        $this->add($attribute);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getAttributes()
    {
        return $this->getIterator();
    }

    /**
     * Creates a new instance from the specified elements.
     *
     * This method is provided for derived classes to specify how a new
     * instance should be created when constructor semantics have changed.
     *
     * @param array $elements Elements.
     *
     * @return static
     */
    private function createFrom(array $elements)
    {
        return new static($elements);
    }

    /**
     * checks if the Element implements the AttributeInterface
     *
     * @param mixed $element
     * @throws InvalidArgumentException
     */
    private function checkAttributeInterfaceImplementation($element)
    {
        if (!$element instanceof AttributeInterface) {
            throw new InvalidArgumentException(
                'Element added to an AttributeCollection must be implementing AttributeInterface'
            );
        }
    }

    /**
     * checks if the attribute already exists
     *
     * @param AttributeInterface $element
     * @throws DoubleAttributeException
     */
    private function existsAttribute(AttributeInterface $element)
    {
        if ($this->containsKey($element->getName())) {
            throw new DoubleAttributeException(
                'An attribute wit the name ' . $element->getName() . ' already exists for this element'
            );
        }
    }
}