<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 12.09.16 20:33
 */

namespace Dknx01\ObjectXml\Collection;

use Dknx01\ObjectXml\Exception\InvalidArgumentException;
use Dknx01\ObjectXml\Restriction\RestrictionInterface;

/**
 * @inheritdoc
 */
class RestrictionCollection implements CollectionInterface, RestrictionInterface
{
    /**
     * @var RestrictionInterface[]
     */
    private $elements;

    /**
     * @param RestrictionInterface[] $elements
     */
    public function __construct(array $elements = array())
    {
        $this->elements = $elements;
    }

    /**
     * @inheritDoc
     *
     * @throws InvalidArgumentException
     */
    public function add($element)
    {
        if ($element instanceof RestrictionInterface) {
            $this->elements[] = $element;
        } elseif (!is_null($element)) {
            throw new InvalidArgumentException(
                'Value must be instance of \Dknx01\ObjectXml\Restriction\RestrictionInterface'
            );
        }
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function clear()
    {
        $this->elements = array();
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isEmpty()
    {
        return empty($this->elements);
    }

    /**
     * @inheritDoc
     */
    public function remove($key)
    {
        if (array_key_exists($key, $this->elements)) {
            unset($this->elements[$key]);
        }
        return $this;
    }

    /**
     * Checks whether the collection contains an element of the specified class.
     *
     * @param string $typeClass The class to check for.
     *
     * @return boolean TRUE if the collection contains an element  of the specified class,
     *                 FALSE otherwise.
     */
    public function containsRestrictionType($typeClass)
    {
        return $this->exists(function ($key, $element) use ($typeClass) {
            return $element instanceof $typeClass;
        });
    }

    /**
     * @inheritDoc
     */
    public function get($key)
    {
        return array_key_exists($key, $this->elements) ? $this->elements[$key] : null;
    }

    /**
     * @inheritDoc
     *
     * @throws InvalidArgumentException
     */
    public function set($key, $value)
    {
        if (!$value instanceof RestrictionInterface) {
            throw new InvalidArgumentException(
                'Value must be instance of \Dknx01\ObjectXml\Restriction\RestrictionInterface'
            );
        }
        $this->elements[$key] = $value;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function exists(\Closure $p)
    {
        foreach ($this->elements as $key => $element) {
            if ($p($key, $element)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    public function filter(\Closure $p)
    {
        return $this->createFrom(array_filter($this->elements, $p));
    }

    /**
     * @inheritDoc
     */
    public function map(\Closure $func)
    {
        return $this->createFrom(array_map($func, $this->elements));
    }

    /**
     * @inheritDoc
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->elements);
    }

    /**
     * @inheritDoc
     */
    public function count()
    {
        return count($this->elements);
    }

    /**
     * @inheritDoc
     */
    public function validate($value)
    {
        foreach ($this->elements as $element) {
            $element->validate($value);
        }
    }

    /**
     * Creates a new instance from the specified elements.
     *
     * This method is provided for derived classes to specify how a new
     * instance should be created when constructor semantics have changed.
     *
     * @param array $elements Elements.
     *
     * @return static
     */
    private function createFrom(array $elements)
    {
        return new static($elements);
    }
}