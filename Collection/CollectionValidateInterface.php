<?php
/**
 * interface to notify that the whole collection should be validated before creating the xml elements
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 2017-08-30
 */

namespace Dknx01\ObjectXml\Collection;

/**
 * @inheritdoc
 */
interface CollectionValidateInterface
{
    /**
     * the method to validate the collection for valid content
     */
    public function validateCollection();
}
