<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 12.09.16 20:24
 */

namespace Dknx01\ObjectXml\Collection;

/**
 * Interface CollectionInterface
 */
interface CollectionInterface extends \Countable, \IteratorAggregate
{
    /**
     * Adds an element at the end of the collection.
     *
     * @param mixed $element The element to add.
     *
     * @return CollectionInterface
     */
    public function add($element);

    /**
     * Clears the collection, removing all elements.
     *
     * @return CollectionInterface
     */
    public function clear();

    /**
     * Checks whether the collection is empty (contains no elements).
     *
     * @return boolean TRUE if the collection is empty, FALSE otherwise.
     */
    public function isEmpty();

    /**
     * Removes the element at the specified index from the collection.
     *
     * @param string|integer $key The kex/index of the element to remove.
     *
     * @return CollectionInterface.
     */
    public function remove($key);

    /**
     * Gets the element at the specified key/index.
     *
     * @param string|integer $key The key/index of the element to retrieve.
     *
     * @return mixed
     */
    public function get($key);

    /**
     * Sets an element in the collection at the specified key/index.
     *
     * @param string|integer $key   The key/index of the element to set.
     * @param mixed          $value The element to set.
     *
     * @return CollectionInterface
     */
    public function set($key, $value);

    /**
     * Tests for the existence of an element that satisfies the given predicate.
     *
     * @param \Closure $p The predicate.
     *
     * @return boolean TRUE if the predicate is TRUE for at least one element, FALSE otherwise.
     */
    public function exists(\Closure $p);

    /**
     * Returns all the elements of this collection that satisfy the predicate p.
     * The order of the elements is preserved.
     *
     * @param \Closure $p The predicate used for filtering.
     *
     * @return CollectionInterface A collection with the results of the filter operation.
     */
    public function filter(\Closure $p);

    /**
     * Applies the given function to each element in the collection and returns
     * a new collection with the elements returned by the function.
     *
     * @param \Closure $func
     *
     * @return CollectionInterface
     */
    public function map(\Closure $func);
}