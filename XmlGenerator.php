<?php
/**
 * @author Dknx01 <e.witthauer@gmail.com>
 * @since 11.09.16 20:00
 */

namespace Dknx01\ObjectXml;

use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Collection\CollectionInterface;
use Dknx01\ObjectXml\Collection\CollectionValidateInterface;
use Dknx01\ObjectXml\Element\AttributeInterface;
use Dknx01\ObjectXml\Element\AttributesAwareInterface;
use Dknx01\ObjectXml\Element\BaseElement;
use Dknx01\ObjectXml\Element\CDataAwareInterface;
use Dknx01\ObjectXml\Element\CollectionElement;
use Dknx01\ObjectXml\Element\ElementInterface;
use Dknx01\ObjectXml\Element\NamespaceAwareInterface;
use Dknx01\ObjectXml\Element\RootNamespaceElement;
use Dknx01\ObjectXml\Exception\XmlValidationException;

/**
 * generator used for generating the xml string for an given ObjectXml and may be validated against an xsd file
 */
class XmlGenerator implements AttributesAwareInterface
{
    /**
     * @var string
     */
    private $version;

    /**
     * @var string
     */
    private $encoding;

    /**
     * @var bool
     */
    private $formatOutput;

    /**
     * @var bool
     */
    private $xmlStandalone;

    /**
     * @var string
     */
    private $xsd;

    /**
     * @var AttributeCollection
     */
    private $rootAttributes;

    /**
     * XmlGenerator constructor.
     * @param null|string $version
     * @param null|string $enconding
     * @param bool $standAlone
     * @param bool $formatOutput
     */
    public function __construct($version = null, $enconding = null, $standAlone = true, $formatOutput = true)
    {
        $this->version = $version ?: '1.0';
        $this->encoding = $enconding ?: 'utf-8';
        $this->formatOutput = $formatOutput;
        $this->xmlStandalone = $standAlone;
        $this->rootAttributes = new AttributeCollection();
    }

    /**
     * set the path to a xsd file the generated xml will be validated against
     *
     * @param string $xsd
     *
     * @return XmlGenerator
     */
    public function setXsd($xsd)
    {
        $this->xsd = $xsd;
        return $this;
    }

    /**
     * Creates the xml string for the given ObjectXml and root element name.
     * If there is a xsd file set the string will be validated against this file.
     *
     * @param string $rootNodeName
     * @param ObjectXml $objectXml
     *
     * @return string
     */
    public function createXml($rootNodeName, ObjectXml $objectXml)
    {
        $dom = new \DOMDocument($this->version, $this->encoding);
        $dom->formatOutput = $this->formatOutput;
        $dom->xmlStandalone = $this->xmlStandalone;

        $root = $dom->createElement($rootNodeName);
        /** @var AttributeInterface $attribute */
        foreach ($this->rootAttributes as $attribute) {
            $rootAttribute = $dom->createAttribute($attribute->getName());
            $rootAttribute->value = $attribute->getValue();
            $root->setAttribute($attribute->getName(), $attribute->getValue());
        }

        /** @var ElementInterface|BaseElement $element */
        foreach ($objectXml as $element) {
            $root = $this->addElements($element, $dom, $root);
        }

        $dom->appendChild($root);

        return $this->validate($dom->saveXML());
    }

    /**
     * Creates the xml string for the given ObjectXml and root element name. The root element is a RootNamespace
     * element with optional attributes.
     * If there is a xsd file set the string will be validated against this file.
     *
     * @param RootNamespaceElement $rootNamespaceElement
     * @param ObjectXml $objectXml
     *
     * @return string
     */
    public function createXmlWithRootNamepsace(RootNamespaceElement $rootNamespaceElement, ObjectXml $objectXml)
    {
        $dom = new \DOMDocument($this->version, $this->encoding);
        $dom->formatOutput = $this->formatOutput;
        $dom->xmlStandalone = $this->xmlStandalone;

        $root = $dom->createElementNS(
            $rootNamespaceElement->getNamespaceData()->getNamespace(),
            $rootNamespaceElement->getNamespaceData()->getQualifiedName()
        );
        /** @var AttributeInterface $attribute */
        foreach ($rootNamespaceElement->getAttributes() as $attribute) {
            $rootAttribute = $dom->createAttribute($attribute->getName());
            $rootAttribute->value = $attribute->getValue();
            $root->setAttribute($attribute->getName(), $attribute->getValue());
        }

        /** @var AttributeInterface $attribute */
        foreach ($this->rootAttributes as $attribute) {
            $rootAttribute = $dom->createAttribute($attribute->getName());
            $rootAttribute->value = $attribute->getValue();
            $root->setAttribute($attribute->getName(), $attribute->getValue());
        }

        /** @var ElementInterface|BaseElement $element */
        foreach ($objectXml as $element) {
            $root = $this->addElements($element, $dom, $root);
        }

        $dom->appendChild($root);

        return $this->validate($dom->saveXML());
    }

    /**
     * @inheritDoc
     */
    public function addAttribute(AttributeInterface $attribute)
    {
        $this->rootAttributes->add($attribute);

        return $this;
    }

    /**
     * @param AttributeInterface $attribute
     * @return $this|XmlGenerator
     */
    public function addRootAttribute(AttributeInterface $attribute)
    {
        return $this->addAttribute($attribute);
    }

    /**
     * @inheritDoc
     */
    public function getAttributes()
    {
        return $this->rootAttributes;
    }

    /**
     * creates an new DOMElement and adds all given attributes
     *
     * @param \DOMDocument $dom
     * @param $tagName
     * @param $content
     * @param AttributeCollection $attributes
     * @param NamespaceData|null $nameSpace
     *
     * @return \DOMElement
     */
    private function createElement(\DOMDocument $dom, $tagName, $content, AttributeCollection $attributes, NamespaceData $nameSpace = null)
    {
        if ($nameSpace instanceof NamespaceData) {
            $childElement = $dom->createElementNS($nameSpace->getNamespace(), $nameSpace->getQualifiedName(), $content);
        } else {
            $childElement = $dom->createElement($tagName, $content);
        }
        if ($attributes instanceof AttributesAwareInterface && !$attributes->isEmpty()) {
            /** @var AttributeInterface $attribute */
            foreach ($attributes->getIterator() as $attribute) {
                $domAttribute = $dom->createAttribute($attribute->getName());
                $domAttribute->value = $attribute->getValue();
                $childElement->setAttribute($attribute->getName(), $attribute->getValue());
            }
        }
        return $childElement;
    }

    /**
     * add an element to the $root DOMElement
     *
     * @param ElementInterface|BaseElement $element
     * @param \DOMDocument $domDocument
     * @param \DOMElement $domElement
     *
     * @return \DOMElement
     */
    private function addElements(ElementInterface $element, \DOMDocument $domDocument, \DOMElement $domElement)
    {
        if ($element instanceof CollectionInterface) {
            /** @var CollectionElement $element */
            $domElement = $this->addCollectionElement($element, $domDocument, $domElement);
        } elseif ($element instanceof CDataAwareInterface) {
            $domElement->appendChild($this->createCdataElement($domDocument, $element));
        } else {
            $domElement->appendChild($this->createElement(
                $domDocument,
                $element->getTagName(),
                $element->getContent(),
                $element->getAttributes(),
                $element instanceof NamespaceAwareInterface ? $element->getNamespaceData() : null
            ));
        }

        return $domElement;
    }

    /**
     * validates the xml string against the xsd file, if one is set
     *
     * @param string $xml
     *
     * @return string
     *
     * @throws XmlValidationException
     */
    private function validate($xml)
    {
        if (!is_null($this->xsd)) {
            libxml_use_internal_errors(true);

            $dom = new \DOMDocument();
            $dom->loadXML($xml);
            if (!$dom->schemaValidate($this->xsd)) {
                $errorMessages = array();
                /** @var \LibXMLError $error */
                foreach (libxml_get_errors() as $error) {
                    $errorMessages[] = $error->message;
                }
                array_unique($errorMessages);
                throw new XmlValidationException(
                    'Generated XML is not valid against the XSD file (' . $this->xsd . '):' . PHP_EOL .
                    implode(PHP_EOL, $errorMessages)
                );
            }
            libxml_use_internal_errors(false);
        }

        return $xml;
    }

    /**
     * @param CollectionElement $element
     * @param \DOMDocument $dom
     * @param \DOMElement $root
     *
     * @return \DOMElement
     */
    private function addCollectionElement(CollectionElement $element, \DOMDocument $dom, \DOMElement $root)
    {
        if ($element instanceof CollectionValidateInterface) {
            $element->validateCollection();
        }

        $childElement = $this->createElement(
            $dom,
            $element->getTagName(),
            null,
            $element->getAttributes()
        );
        /** @var BaseElement $item */
        foreach ($element->getIterator() as $item) {
            $this->addElements($item, $dom, $childElement);
        }

        $root->appendChild($childElement);

        return $root;
    }

    /**
     * @param \DOMDocument $domDocument
     * @param ElementInterface $element
     * @return \DOMCdataSection
     */
    private function createCdataElement(\DOMDocument $domDocument, ElementInterface $element)
    {
        return $domDocument->createCDATASection($element->getContent());
    }
}
