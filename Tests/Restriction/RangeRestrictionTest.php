<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 30.09.16 21:59
 */

namespace Dknx01\ObjectXml\Tests\Restriction;

use Dknx01\ObjectXml\Restriction\RangeRestriction;

/**
 * @inheritdoc
 */
class RangeRestrictionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function provideValidData()
    {
        return array(
            array(0),
            array(1),
            array(10),
            array(-5)
        );
    }

    /**
     * @dataProvider provideValidData
     */
    public function testRangeRestriction($value)
    {
        $restriction = new RangeRestriction(-10, 10);
        $restriction->validate($value);
    }

    /**
     * @return array
     */
    public function provideInvalidData()
    {
        return array(
            array('bla'),
            array(true),
            array(-10),
            array(45.99)
        );
    }

    /**
     * @dataProvider provideInvalidData
     *
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     */
    public function testRangeRestrictionWithException($value)
    {
        $restriction = new RangeRestriction(-5, 5);
        $restriction->validate($value);
    }
}
