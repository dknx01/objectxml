<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 01.10.16 10:55
 */

namespace Dknx01\ObjectXml\Tests\Restriction;
use Dknx01\ObjectXml\Restriction\RegexRestriction;

/**
 * @inheritdoc
 */
class RegexRestrictionTest extends \PHPUnit_Framework_TestCase
{
    public function testRegexRestriction()
    {
        $pattern = '/^\d+$/';
        $restriction = new RegexRestriction($pattern);
        $restriction->validate(55959);
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     * @expectedExceptionMessage fooo does not match the pattern /^\d+$/
     */
    public function testRegexRestrictionWithException()
    {
        $pattern = '/^\d+$/';
        $restriction = new RegexRestriction($pattern);
        $restriction->validate('fooo');
    }
}
