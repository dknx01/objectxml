<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 05.10.16 20:32
 */

namespace Dknx01\ObjectXml\Tests\Restriction;

use Dknx01\ObjectXml\Restriction\GenericStringRestriction;

/**
 * @inheritdoc
 */
class GenericStringRestrictionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function provideValidData()
    {
        return array(
            array(''),
            array(1),
            array(100.00),
            array('blabla'),
            array(true),
            array(null)
        );
    }

    /**
     * @dataProvider provideValidData
     */
    public function testGenericStringRestriction($value)
    {
        $restriction = new GenericStringRestriction();
        $restriction->validate($value);
    }

    /**
     * @return array
     */
    public function provideInvalidData()
    {
        return array(
            array(array()),
            array(new \stdClass()),
            array(fopen(__FILE__, 'r'))
        );
    }

    /**
     * @dataProvider provideInvalidData
     *
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     * @expectedExceptionMessage The value is not valid generic string.
     */
    public function testGenericStringRestrictionWithException($value)
    {
        $restriction = new GenericStringRestriction();
        $restriction->validate($value);
    }
}
