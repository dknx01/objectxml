<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 01.10.16 11:03
 */

namespace Dknx01\ObjectXml\Tests\Restriction;

use Dknx01\ObjectXml\Restriction\TimeRestriction;

/**
 * @inheritdoc
 */
class TimeRestrictionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function provideValidData()
    {
        return array(
            array('00:00:00'),
            array('08:08:19'),
            array('20:08:19')
        );
    }

    /**
     * @dataProvider provideValidData
     */
    public function testTimeRestriction($value)
    {
        $restriction = new TimeRestriction();
        $restriction->validate($value);
    }

    /**
     * @return array
     */
    public function provideInvalidData()
    {
        return array(
            array('00:00'),
            array('24:00:59'),
            array('10:11:60'),
            array(45.99),
            array(null),
            array(''),
            array('a')
        );
    }

    /**
     * @dataProvider provideInvalidData
     *
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     */
    public function testTimeRestrictionWithException($value)
    {
        $restriction = new TimeRestriction();
        $restriction->validate($value);
    }
}
