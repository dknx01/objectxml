<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 01.10.16 12:06
 */

namespace Dknx01\ObjectXml\Tests\Restriction;

use Dknx01\ObjectXml\Restriction\DateTimeRestriction;

/**
 * @inheritdoc
 */
class DateTimeRestrictionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function provideValidData()
    {
        return array(
            array('815-01-03 12:00:55'),
            array('2016-11-06 00:00:00'),
            array('01-01-01 23:45:35')
        );
    }

    /**
     * @dataProvider provideValidData
     */
    public function testDateTimeRestriction($value)
    {
        $restriction = new DateTimeRestriction();
        $restriction->validate($value);
    }

    /**
     * @return array
     */
    public function provideInvalidData()
    {
        return array(
            array('2016-11-06 00:00'),
            array('01.01.1980'),
            array('12:13:52'),
            array(45.99),
            array(null),
            array(''),
            array('a')
        );
    }

    /**
     * @dataProvider provideInvalidData
     *
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     */
    public function testDateTimeRestrictionWithException($value)
    {
        $restriction = new DateTimeRestriction();
        $restriction->validate($value);
    }
}
