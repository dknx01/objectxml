<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 30.09.16 21:41
 */

namespace Dknx01\ObjectXml\Tests\Restriction;

use Dknx01\ObjectXml\Restriction\BooleanRestriction;

/**
 * @inheritdoc
 */
class BooleanRestrictionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function provideValidData()
    {
        return array(
            array(0),
            array(1),
            array(true),
            array(false)
        );
    }

    /**
     * @dataProvider provideValidData
     */
    public function testBooleanRestriction($value)
    {
        $restriction = new BooleanRestriction();
        $restriction->validate($value);
    }

    /**
     * @return array
     */
    public function provideInvalidData()
    {
        return array(
            array('bla'),
            array(5),
            array(new \stdClass()),
            array(45.99)
        );
    }

    /**
     * @dataProvider provideInvalidData
     *
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     */
    public function testBooleanRestrictionWithException($value)
    {
        $restriction = new BooleanRestriction();
        $restriction->validate($value);
    }
}
