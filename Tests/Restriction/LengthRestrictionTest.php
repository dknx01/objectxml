<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 30.09.16 22:10
 */

namespace Dknx01\ObjectXml\Tests\Restriction;

use Dknx01\ObjectXml\Restriction\LengthRestriction;

/**
 * @inheritdoc
 */
class LengthRestrictionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function provideValidData()
    {
        return array(
            array('a'),
            array('hallo')
        );
    }

    /**
     * @dataProvider provideValidData
     */
    public function testLengthRestriction($value)
    {
        $restriction = new LengthRestriction(1, 10);
        $restriction->validate($value);
    }

    /**
     * @return array
     */
    public function provideInvalidData()
    {
        return array(
            array('blablablablabla'),
            array(true),
            array(-10),
            array(45.99),
            array(null),
            array(''),
            array('a')
        );
    }

    /**
     * @dataProvider provideInvalidData
     *
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     */
    public function testLengthRestrictionWithException($value)
    {
        $restriction = new LengthRestriction(2, 10);
        $restriction->validate($value);
    }
}
