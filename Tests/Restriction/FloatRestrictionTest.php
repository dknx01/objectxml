<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 30.09.16 21:55
 */

namespace Dknx01\ObjectXml\Tests\Restriction;

use Dknx01\ObjectXml\Restriction\FloatRestriction;

/**
 * @inheritdoc
 */
class FloatRestrictionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function provideValidData()
    {
        return array(
            array(0.0),
            array(5.66),
            array(-5.99),
            array(-5.0)
        );
    }

    /**
     * @dataProvider provideValidData
     */
    public function testFloatRestriction($value)
    {
        $restriction = new FloatRestriction();
        $restriction->validate($value);
    }

    /**
     * @return array
     */
    public function provideInvalidData()
    {
        return array(
            array('bla'),
            array(true),
            array(new \stdClass()),
            array(42)
        );
    }

    /**
     * @dataProvider provideInvalidData
     *
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     */
    public function testFloatRestrictionWithException($value)
    {
        $restriction = new FloatRestriction();
        $restriction->validate($value);
    }
}
