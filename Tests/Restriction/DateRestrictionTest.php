<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 01.10.16 11:44
 */

namespace Dknx01\ObjectXml\Tests\Restriction;

use Dknx01\ObjectXml\Restriction\DateRestriction;

/**
 * @inheritdoc
 */
class DateRestrictionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function provideValidData()
    {
        return array(
            array('815-01-03'),
            array('2016-11-06'),
            array('01-01-01')
        );
    }

    /**
     * @dataProvider provideValidData
     */
    public function testDateRestriction($value)
    {
        $restriction = new DateRestriction();
        $restriction->validate($value);
    }

    /**
     * @return array
     */
    public function provideInvalidData()
    {
        return array(
            array('00:00'),
            array('01.01.1980'),
            array('12-24'),
            array(45.99),
            array(null),
            array(''),
            array('a')
        );
    }

    /**
     * @dataProvider provideInvalidData
     *
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     */
    public function testDateRestrictionWithException($value)
    {
        $restriction = new DateRestriction();
        $restriction->validate($value);
    }
}
