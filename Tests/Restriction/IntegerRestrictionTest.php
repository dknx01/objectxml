<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 30.09.16 21:51
 */

namespace Dknx01\ObjectXml\Tests\Restriction;

use Dknx01\ObjectXml\Restriction\IntegerRestriction;

/**
 * @inheritdoc
 */
class IntegerRestrictionTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function provideValidData()
    {
        return array(
            array(0),
            array(1),
            array(100),
            array(-5)
        );
    }

    /**
     * @dataProvider provideValidData
     */
    public function testIntegerRestriction($value)
    {
        $restriction = new IntegerRestriction();
        $restriction->validate($value);
    }

    /**
     * @return array
     */
    public function provideInvalidData()
    {
        return array(
            array('bla'),
            array(true),
            array(new \stdClass()),
            array(45.99)
        );
    }

    /**
     * @dataProvider provideInvalidData
     *
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     */
    public function testIntegerRestrictionWithException($value)
    {
        $restriction = new IntegerRestriction();
        $restriction->validate($value);
    }
}
