<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 01.10.16 18:01
 */

namespace Dknx01\ObjectXml\Tests;

use Dknx01\ObjectXml\Element\ElementInterface;
use Dknx01\ObjectXml\Element\StringElement;
use Dknx01\ObjectXml\ObjectXml;

class ObjectXmlTest extends \PHPUnit_Framework_TestCase
{
    public function testObjectXml()
    {
        $objectXml = new ObjectXml();

        $objectXml->add(new StringElement('berg', 'zoid'));

        self::assertEquals(1 ,$objectXml->count());

        /** @var ElementInterface $element */
        foreach ($objectXml->getIterator() as $element) {
            self::assertEquals('zoid', $element->getTagName());
            self::assertEquals('berg', $element->getContent());
        }
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidArgumentException
     */
    public function testObjectXmlWithInvalidElement()
    {
        $objectXml = new ObjectXml();

        $objectXml->add('foo');
    }
}
