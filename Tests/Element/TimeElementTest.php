<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 01.10.16 17:03
 */

namespace Dknx01\ObjectXml\Tests\Element;

use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Element\TimeElement;
use Dknx01\ObjectXml\Restriction\RestrictedValuesRestriction;

/**
 * @inheritdoc
 */
class TimeElementTest extends \PHPUnit_Framework_TestCase
{
    public function testBooleanElement()
    {
        $element = new TimeElement('12:13:59', 'zoidberg');

        self::assertEquals('12:13:59', $element->getContent());
        self::assertEquals('zoidberg', $element->getTagName());
        self::assertEquals(new AttributeCollection(), $element->getAttributes());
    }

    public function testTimeElementWithRestriction()
    {
        $element = new TimeElement('12:13:59', 'zoidberg', null, new RestrictedValuesRestriction(array('12:13:59')));

        self::assertEquals('12:13:59', $element->getContent());
        self::assertEquals('zoidberg', $element->getTagName());
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     */
    public function testTimeElementWithInvalidRestriction()
    {
        $element = new TimeElement('bla', 'zoidberg');
    }
}
