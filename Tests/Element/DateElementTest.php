<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 01.10.16 16:56
 */

namespace Dknx01\ObjectXml\Tests\Element;

use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Element\DateElement;
use Dknx01\ObjectXml\Restriction\RegexRestriction;

/**
 * @inheritdoc
 */
class DateElementTest extends \PHPUnit_Framework_TestCase
{
    public function testBooleanElement()
    {
        $element = new DateElement('2016-10-03', 'zoidberg');

        self::assertEquals('2016-10-03', $element->getContent());
        self::assertEquals('zoidberg', $element->getTagName());
        self::assertEquals(new AttributeCollection(), $element->getAttributes());
    }

    public function testDateElementWithRestriction()
    {
        $element = new DateElement('2016-10-03', 'zoidberg', null, new RegexRestriction('/^2016-.*/'));

        self::assertEquals('2016-10-03', $element->getContent());
        self::assertEquals('zoidberg', $element->getTagName());
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     */
    public function testDateElementWithInvalidRestriction()
    {
        $element = new DateElement('bla', 'zoidberg');
    }
}
