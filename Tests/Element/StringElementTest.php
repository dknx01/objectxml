<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 29.09.16 20:38
 */

namespace dknx01\ObjectXml\Tests\Element;

use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Element\StringElement;

class StringElementTest extends \PHPUnit_Framework_TestCase
{
    public function testStringElement()
    {
        $stringElement = new StringElement('whooop', 'zoidberg');

        self::assertEquals('whooop', $stringElement->getContent());
        self::assertEquals('zoidberg', $stringElement->getTagName());
        self::assertEquals(new AttributeCollection(), $stringElement->getAttributes());
    }
}
