<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 05.10.16 20:42
 */

namespace Dknx01\ObjectXml\Tests\Element;


use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Element\BaseAttribute;
use Dknx01\ObjectXml\Element\CDataElement;
use Dknx01\ObjectXml\Restriction\BooleanRestriction;

class CDataElementTest extends \PHPUnit_Framework_TestCase
{
    public function testCDataElement()
    {
        $cDataElement = new CDataElement(true);

        self::assertEquals(true, $cDataElement->getContent());
        self::assertNull($cDataElement->getTagName());
        self::assertEquals(new AttributeCollection(), $cDataElement->getAttributes());
    }

    public function testCDataElementWithRestriction()
    {
        $CDataElement = new CDataElement(true, null, null, new BooleanRestriction());

        self::assertEquals(true, $CDataElement->getContent());
        self::assertNull($CDataElement->getTagName());
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidTagNameException
     * @expectedExceptionMessage A CDATA element must not have a tag name
     */
    public function testCDataElementWithInvalidTagName()
    {
        $cDataElement = new CDataElement('bla', 'zoidberg');
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidAttributeException
     * @expectedExceptionMessage A CDATA element must not have any attributes
     */
    public function testCDataElementWithInvalidAttributes()
    {
        $attributes = new AttributeCollection();
        $attributes->add(new BaseAttribute('Foo', 'Bar'));
        $cDataElement = new CDataElement('bla', null, $attributes);
    }
}
