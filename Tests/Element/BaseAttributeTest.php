<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 29.09.16 20:17
 */

namespace dknx01\ObjectXml\Tests\Element;

use Dknx01\ObjectXml\Element\BaseAttribute;
use Dknx01\ObjectXml\Restriction\LengthRestriction;

class BaseAttributeTest extends \PHPUnit_Framework_TestCase
{
    public function testBaseAttribute()
    {
        $baseAttribute = new BaseAttribute('foo', 'bar', new LengthRestriction(1, 5));

        self::assertEquals('foo', $baseAttribute->getName());
        self::assertEquals('bar', $baseAttribute->getValue());
    }

    /**
     * @return array
     */
    public function provideNames()
    {
        return array(
            array(null),
            array('')
        );
    }

    /**
     * @dataProvider provideNames
     * @param mixed $name
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidNameException
     * @expectedExceptionMessage Attribute name cannot be empty
     */
    public function testBaseAttributeWithInvalidName($name)
    {
        $baseAttribute = new BaseAttribute($name, 'bar', new LengthRestriction(1, 5));
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     * @expectedExceptionMessage The text cannot be longer than 1
     */
    public function testBaseAttributeWithInvalidRestriction()
    {
        $baseAttribute = new BaseAttribute('bar', 'bar', new LengthRestriction(1, 1));
    }
}
