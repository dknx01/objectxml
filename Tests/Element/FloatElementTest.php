<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 01.10.16 16:31
 */

namespace Dknx01\ObjectXml\Tests\Element;

use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Element\FloatElement;
use Dknx01\ObjectXml\Restriction\RangeRestriction;

/**
 * @inheritdoc
 */
class FloatElementTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @return array
     */
    public function provideValidData()
    {
        return array(
            array(42.47),
            array(-42.00)
        );
    }

    /**
     * @dataProvider provideValidData
     */
    public function testFloatElement($value)
    {
        $element = new FloatElement($value, 'zoidberg');
        self::assertEquals($value, $element->getContent());
        self::assertEquals('zoidberg', $element->getTagName());
        self::assertEquals(new AttributeCollection(), $element->getAttributes());
    }

    public function testFloatElementWithRestriction()
    {
        $element = new FloatElement(42.00, 'zoidberg', null, new RangeRestriction(0, 100));

        self::assertEquals(42.00, $element->getContent());
        self::assertEquals('zoidberg', $element->getTagName());
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     */
    public function testFloatElementWithInvalidRestriction()
    {
        $element = new FloatElement('bla', 'zoidberg');
    }
}
