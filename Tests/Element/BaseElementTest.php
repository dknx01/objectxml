<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 29.09.16 20:25
 */

namespace dknx01\ObjectXml\Tests\Element;

use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Element\BaseAttribute;
use Dknx01\ObjectXml\Element\BaseElement;
use Dknx01\ObjectXml\Restriction\LengthRestriction;

class BaseElementTest extends \PHPUnit_Framework_TestCase
{
    public function testBaseElement()
    {
        $baseElement = new BaseElement('whooop', 'zoidberg', null, new LengthRestriction(1, 10));

        self::assertEquals('whooop', $baseElement->getContent());
        self::assertEquals('zoidberg', $baseElement->getTagName());
        self::assertEquals(new AttributeCollection(), $baseElement->getAttributes());

        $baseElement->addAttribute(new BaseAttribute('foo', 'bar'));
        self::assertEquals(1, $baseElement->getAttributes()->count());
    }

    /**
     * @return array
     */
    public function provideNames()
    {
        return array(
            array(null),
            array('')
        );
    }

    /**
     * @dataProvider provideNames
     * @param mixed $name
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidTagNameException
     * @expectedExceptionMessage Tag name cannot be empty
     */
    public function testBaseAttributeWithInvalidName($name)
    {
        $baseAttribute = new BaseElement('bar', $name);
    }

}
