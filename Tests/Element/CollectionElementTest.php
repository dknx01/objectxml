<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 01.10.16 17:05
 */

namespace Dknx01\ObjectXml\Tests\Element;
use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Element\BaseAttribute;
use Dknx01\ObjectXml\Element\BooleanElement;
use Dknx01\ObjectXml\Element\CollectionElement;
use Dknx01\ObjectXml\Element\ElementInterface;
use Dknx01\ObjectXml\Element\StringElement;

/**
 * @inheritdoc
 */
class CollectionElementTest extends \PHPUnit_Framework_TestCase
{
    public function testCollectionElement()
    {
        $subElement = new StringElement('foo', 'bar');

        $element = new CollectionElement(
            $subElement,
            'collection'
        );

        self::assertFalse($element->isEmpty());
        self::assertTrue($element->containsKey('bar'));
        self::assertNull($element->get('bla'));
        self::assertEquals($subElement, $element->get('bar'));

        $element->set('zoid', new BooleanElement(true, 'berg'));

        self::assertEquals(2, $element->count());

        foreach ($element as $item) {
            //nothing to do here
        }

        foreach ($element->getIterator() as $item) {
            //nothing to do here
        }

        $attribute1 = new BaseAttribute('nothing', 'to do');

        $element->addAttribute($attribute1);

        foreach ($element->getAttributes() as $attribute) {
            //nothing to do
        }

        $key = 'berg';
        self::assertEquals(
            new CollectionElement(
                new BooleanElement(true, 'berg'),
                'collection',
                new AttributeCollection(array($attribute1))
            ),
            $element->filter(function ($element) use ($key) {
                /** @var ElementInterface $element */
                if ($element->getTagName() === $key) {
                    return $element;
                }
            })
        );

        self::assertEquals(
            new CollectionElement(
                new BooleanElement(true, 'berg'),
                'collection',
                new AttributeCollection(array($attribute1))
            ),
            $element->map(function ($element) use ($key) {
                /** @var ElementInterface $element */
                if ($element->getTagName() === $key) {
                    return $element;
                }
            })
        );

        self::assertTrue(
            $element->exists(function ($k, $element) use ($key) {
                /** @var ElementInterface $element */
                return $element->getTagName() === $key;
            })
        );

        $key2 = 'whoop';

        self::assertFalse(
            $element->exists(function ($k, $element) use ($key2) {
                /** @var ElementInterface $element */
                return $element->getTagName() === $key2;
            })
        );

        $element->remove('bar');

        $element->clear();
        self::assertEquals(0, $element->count());
    }
}
