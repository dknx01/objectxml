<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 29.09.16 20:40
 */

namespace dknx01\ObjectXml\Tests\Element;

use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Element\BooleanElement;
use Dknx01\ObjectXml\Restriction\BooleanRestriction;

class BooleanElementTest extends \PHPUnit_Framework_TestCase
{
    public function testBooleanElement()
    {
        $booleanElement = new BooleanElement(true, 'zoidberg');

        self::assertEquals(true, $booleanElement->getContent());
        self::assertEquals('zoidberg', $booleanElement->getTagName());
        self::assertEquals(new AttributeCollection(), $booleanElement->getAttributes());
    }

    public function testBooleanElementWithRestriction()
    {
        $booleanElement = new BooleanElement(true, 'zoidberg', null, new BooleanRestriction());

        self::assertEquals(true, $booleanElement->getContent());
        self::assertEquals('zoidberg', $booleanElement->getTagName());
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     */
    public function testBooleanElementWithInvalidRestriction()
    {
        $booleanElement = new BooleanElement('bla', 'zoidberg');
    }
}
