<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 01.10.16 16:19
 */

namespace Dknx01\ObjectXml\Tests\Element;

use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Element\IntegerElement;
use Dknx01\ObjectXml\Restriction\RangeRestriction;

/**
 * @inheritdoc
 */
class IntegerElementTest extends \PHPUnit_Framework_TestCase
{
    public function testBooleanElement()
    {
        $element = new IntegerElement(42, 'zoidberg');

        self::assertEquals(42, $element->getContent());
        self::assertEquals('zoidberg', $element->getTagName());
        self::assertEquals(new AttributeCollection(), $element->getAttributes());
    }

    public function testIntegerElementWithRestriction()
    {
        $element = new IntegerElement(42, 'zoidberg', null, new RangeRestriction(0, 100));

        self::assertEquals(42, $element->getContent());
        self::assertEquals('zoidberg', $element->getTagName());
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     */
    public function testIntegerElementWithInvalidRestriction()
    {
        $element = new IntegerElement('bla', 'zoidberg');
    }
}
