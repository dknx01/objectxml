<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 01.10.16 17:00
 */

namespace Dknx01\ObjectXml\Tests\Element;

use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Element\DateTimeElement;
use Dknx01\ObjectXml\Restriction\RegexRestriction;
use Dknx01\ObjectXml\Restriction\RestrictedValuesRestriction;

/**
 * @inheritdoc
 */
class DateTimeElementTest extends \PHPUnit_Framework_TestCase
{
    public function testBooleanElement()
    {
        $element = new DateTimeElement('2016-10-03 12:13:59', 'zoidberg');

        self::assertEquals('2016-10-03 12:13:59', $element->getContent());
        self::assertEquals('zoidberg', $element->getTagName());
        self::assertEquals(new AttributeCollection(), $element->getAttributes());
    }

    public function testDateTimeElementWithRestriction()
    {
        $element = new DateTimeElement(
            '2016-10-03 12:13:59',
            'zoidberg',
            null,
            new RestrictedValuesRestriction(array('2016-10-03 12:13:59'))
        );

        self::assertEquals('2016-10-03 12:13:59', $element->getContent());
        self::assertEquals('zoidberg', $element->getTagName());
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidContentException
     */
    public function testDateTimeElementWithInvalidRestriction()
    {
        $element = new DateTimeElement('bla', 'zoidberg');
    }
}
