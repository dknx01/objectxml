<?php

namespace Dknx01\ObjectXml\Tests;

use Dknx01\ObjectXml\Element\NamespaceAwareInterface;
use Dknx01\ObjectXml\Element\StringElement;
use Dknx01\ObjectXml\NamespaceData;

/**
 * @author Erik Witthauer <erik.witthauer@little-bird.de>
 * @since 2017-08-29
 * @copyright 2017 LITTLE BIRD GmbH
 */
class StringNameSpaceElement extends StringElement implements NamespaceAwareInterface
{
    public function getNamespaceData()
    {
        return (new NamespaceData())->setNamespace('https://namespace.test')->setQualifiedName('foo:ns');
    }


}
