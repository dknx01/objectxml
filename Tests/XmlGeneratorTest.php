<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 02.10.16 18:19
 */

namespace Dknx01\ObjectXml\Tests;
use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Element\BaseAttribute;
use Dknx01\ObjectXml\Element\BooleanElement;
use Dknx01\ObjectXml\Element\CDataElement;
use Dknx01\ObjectXml\Element\CollectionElement;
use Dknx01\ObjectXml\Element\RootNamespaceElement;
use Dknx01\ObjectXml\Element\StringElement;
use Dknx01\ObjectXml\NamespaceData;
use Dknx01\ObjectXml\ObjectXml;
use Dknx01\ObjectXml\XmlGenerator;

/**
 * @inheritdoc
 */
class XmlGeneratorTest extends \PHPUnit_Framework_TestCase
{
    public function testXmlGenerator()
    {
        $generator = new XmlGenerator();
        $generator->addAttribute(new BaseAttribute('xmlns', 'http://de.wikipedia.org/wiki/XML_Schema#Beispiel'));

        $objectXml = new ObjectXml();
        $objectXml->add($this->addHead());
        $objectXml->add($this->addBody());
        self::assertEquals($this->expectedXml(), $generator->createXml('doc', $objectXml));

        $generator->setXsd(realpath(__DIR__ . '/../Resources/examples/example.xsd'));
        self::assertEquals($this->expectedXml(), $generator->createXml('doc', $objectXml));

        self::assertInstanceOf('\Dknx01\ObjectXml\Collection\AttributeCollection', $generator->getAttributes());

        $generator->addRootAttribute(new BaseAttribute('foo', 'bar'));
    }

    public function testXmlGenerator2()
    {
        $generator = new XmlGenerator();
        $generator->addRootAttribute(new BaseAttribute('foo', 'bar'));

        $objectXml = new ObjectXml();
        $objectXml->add($this->addHead());
        $objectXml->add($this->addBody(true));
        self::assertEquals($this->expectedXmlWithAttribute(), $generator->createXml('doc', $objectXml));
    }
    public function testXmlGenerator3()
    {
        $generator = new XmlGenerator();

        $objectXml = new ObjectXml();
        $objectXml->add($this->addHead());
        $objectXml->add($this->addBody());
        $objectXml->add($this->addCollection());
        $objectXml->add(new CDataElement('<a cdata string>'));
        self::assertEquals($this->expectedXmlWithCdata(), $generator->createXml('doc', $objectXml));
    }

    public function testXmlGenerator4()
    {
        $generator = new XmlGenerator();
        $generator->addRootAttribute(new BaseAttribute('foo', 'bar'));

        $objectXml = new ObjectXml();
        $objectXml->add($this->addHead());
        $objectXml->add(new StringNameSpaceElement('foooo', 'namspaceStuff'));
        $generator->createXml('doc', $objectXml);
    }

    public function testXmlGenerator5()
    {
        $generator = new XmlGenerator();
        $generator->addRootAttribute(new BaseAttribute('foo', 'bar'));

        $objectXml = new ObjectXml();
        $objectXml->add($this->addHead());

        $namespaceData = new NamespaceData();
        $namespaceData->setNamespace('https://namespace.test')
            ->setQualifiedName('foo:testNs');

        $rootAttribute1 = new BaseAttribute('xmlns:bar', 'http://example.test/test.xsd');
        $rootAttribute2 = new BaseAttribute('version', '007');

        $rootNamespaceElement = new RootNamespaceElement($namespaceData);
        $rootNamespaceElement->addAttribute($rootAttribute1)
            ->addAttribute($rootAttribute2);
        $generator->createXmlWithRootNamepsace($rootNamespaceElement, $objectXml);
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\XmlValidationException
     */
    public function testXmlGeneratorWithXsdError()
    {
        $generator = new XmlGenerator();
        $generator->addAttribute(new BaseAttribute('xmlns', 'http://de.wikipedia.org/wiki/XML_Schema#Beispiel'));

        $objectXml = new ObjectXml();
        $objectXml->add($this->addHead());
        $objectXml->add(new BooleanElement(true, 'blabla'));

        $generator->setXsd(realpath(__DIR__ . '/../Resources/examples/example.xsd'));
        $generator->createXml('doc', $objectXml);
    }

    /**
     * @return CollectionElement
     */
    private function addHead()
    {
        $title = new StringElement('This is a title', 'title');
        return new CollectionElement($title, 'head');
    }

    /**
     * @param bool $withAttribute
     *
     * @return StringElement
     */
    private function addBody($withAttribute = false)
    {
        $attribute = null;
        if ($withAttribute) {
            $attribute = new BaseAttribute('whoop', 'whoop');
        }
        return new StringElement('This is the content', 'body', new AttributeCollection(array($attribute)));
    }

    /**
     * @return CollectionElement
     */
    private function addCollection()
    {
        $collection = new CollectionElement(new StringElement('parabox', 'universe'), 'allTheStuff');

        return $collection;
    }

    /**
     * @return string
     */
    private function expectedXml()
    {
        return <<<XML
<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<doc xmlns="http://de.wikipedia.org/wiki/XML_Schema#Beispiel">
  <head>
    <title>This is a title</title>
  </head>
  <body>This is the content</body>
</doc>

XML;
    }

    /**
     * @return string
     */
    private function expectedXmlWithAttribute()
    {
        return <<<XML
<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<doc foo="bar">
  <head>
    <title>This is a title</title>
  </head>
  <body whoop="whoop">This is the content</body>
</doc>

XML;
    }

    /**
     * @return string
     */
    private function expectedXmlWithCdata()
    {
        return <<<XML
<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<doc><head><title>This is a title</title></head><body>This is the content</body><allTheStuff><universe>parabox</universe></allTheStuff><![CDATA[<a cdata string>]]></doc>

XML;
    }
}
