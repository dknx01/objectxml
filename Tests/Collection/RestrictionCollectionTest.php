<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 27.09.16 21:42
 */

namespace Dknx01\ObjectXml\Tests\Collection;

use Dknx01\ObjectXml\Collection\RestrictionCollection;
use Dknx01\ObjectXml\Restriction\BooleanRestriction;
use Dknx01\ObjectXml\Restriction\IntegerRestriction;
use Dknx01\ObjectXml\Restriction\RangeRestriction;
use Dknx01\ObjectXml\Restriction\RestrictionInterface;

class RestrictionCollectionTest extends \PHPUnit_Framework_TestCase
{
    public function testAttributeCollection()
    {
        $range = new RangeRestriction(0, 10);
        $restrictions = array($range);
        $restrictionCollection = new RestrictionCollection($restrictions);

        self::assertFalse($restrictionCollection->isEmpty());
        self::assertTrue($restrictionCollection->containsRestrictionType(RangeRestriction::class));
        self::assertNull($restrictionCollection->get(99));
        self::assertInstanceOf(RangeRestriction::class, $restrictionCollection->get(0));
        self::assertEquals($range, $restrictionCollection->get(0));

        $restrictionCollection->set(3, new BooleanRestriction());
        self::assertInstanceOf(BooleanRestriction::class, $restrictionCollection->get(3));

        $restrictionCollection->remove(3);
        self::assertNull($restrictionCollection->get(3));

        self::assertEquals(1, $restrictionCollection->count());

        $restrictionCollection->validate(5);

        foreach ($restrictionCollection as $restriction) {
            //nothing to do here
        }

        $key = RangeRestriction::class;
        self::assertEquals(
            new RestrictionCollection(array($range)),
            $restrictionCollection->filter(function ($element) use ($key) {
                /** @var RestrictionInterface $element */
                if ($element instanceof $key) {
                    return $element;
                }
            })
        );

        self::assertEquals(
            new RestrictionCollection(array($range)),
            $restrictionCollection->map(function ($element) use ($key) {
                /** @var RestrictionInterface $element */
                if ($element instanceof $key) {
                    return $element;
                }
            })
        );

        $restrictionCollection->add(new IntegerRestriction());
        $restrictionCollection->add(null);

        self::assertEquals(2, $restrictionCollection->count());
        $restrictionCollection->clear();
        self::assertTrue($restrictionCollection->isEmpty());
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidArgumentException
     * @expectedExceptionMessage Value must be instance of \Dknx01\ObjectXml\Restriction\RestrictionInterface
     */
    public function testRestrictionCollectionWithInvalidElementType()
    {
        $restrictionCollection = new RestrictionCollection();
        $restrictionCollection->add(new \stdClass());
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidArgumentException
     * @expectedExceptionMessage Value must be instance of \Dknx01\ObjectXml\Restriction\RestrictionInterface
     */
    public function testRestrictionCollectionWithInvalidElementTypeForSet()
    {
        $restrictionCollection = new RestrictionCollection();
        $restrictionCollection->set(2, new \stdClass());
    }

    public function testRestrictionCollectionWithNonExistingElement()
    {
        $restrictionCollection = new RestrictionCollection();
        self::assertFalse($restrictionCollection->exists(function ($key, $element) {
            return $element === $key;
        }));
    }
}
