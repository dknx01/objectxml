<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 25.09.16 20:35
 */

namespace dknx01\ObjectXml\Tests\Collection;

use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Element\AttributeInterface;
use Dknx01\ObjectXml\Element\BaseAttribute;

/**
 * @inheritdoc
 */
class AttributeCollectionTest extends \PHPUnit_Framework_TestCase
{
    public function testAttributeCollection()
    {
        $baseAttribute = new BaseAttribute('zoid', 'berg');
        $attributes = array($baseAttribute);
        $attributeCollection = new AttributeCollection($attributes);
        $attributeCollection->addAttribute(new BaseAttribute('foo', 'bla'));

        self::assertFalse($attributeCollection->isEmpty());
        self::assertTrue($attributeCollection->containsKey('zoid'));
        self::assertNull($attributeCollection->get('bla'));
        self::assertEquals($baseAttribute, $attributeCollection->get('zoid'));

        $attributeCollection->set('no', 'one');

        self::assertEquals(3, $attributeCollection->count());

        foreach ($attributeCollection as $attribute) {
            //nothing to do here
        }
        foreach ($attributeCollection->getAttributes() as $attribute) {
            //nothing to do here
        }

        $key = 'zoid';
        self::assertEquals(
            new AttributeCollection($attributes),
            $attributeCollection->filter(function ($element) use ($key) {
                /** @var AttributeInterface $element */
                if ($element->getName() === $key) {
                    return $element;
                }
            })
        );

        self::assertEquals(
            new AttributeCollection($attributes),
            $attributeCollection->map(function ($element) use ($key) {
                /** @var AttributeInterface $element */
                if ($element->getName() === $key) {
                    return $element;
                }
            })
        );

        $attributeCollection->remove('no');
        self::assertEquals(2, $attributeCollection->count());
        $attributeCollection->clear();
        self::assertTrue($attributeCollection->isEmpty());
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\DoubleAttributeException
     * @expectedExceptionMessage An attribute wit the name zoid already exists for this element
     */
    public function testAttributeCollectionWithEqualsAttributes()
    {
        $baseAttribute = new BaseAttribute('zoid', 'berg');
        $attributeCollection = new AttributeCollection(array($baseAttribute, $baseAttribute));
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\DoubleAttributeException
     * @expectedExceptionMessage An attribute wit the name zoid already exists for this element
     */
    public function testAttributeCollectionSetDoubleAttribute()
    {
        $attributeCollection = new AttributeCollection();
        $attributeCollection->set('zoid', 'berg');
        $attributeCollection->set('zoid', 'berg');
    }

    /**
     * @expectedException \Dknx01\ObjectXml\Exception\InvalidArgumentException
     * @expectedExceptionMessage Element added to an AttributeCollection must be implementing AttributeInterface
     */
    public function testAttributeCollectionWithInvalidAttribute()
    {
        $attributeCollection = new AttributeCollection();
        $attributeCollection->add(new \stdClass());
    }
}
