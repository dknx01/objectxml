<?php
/**
 * @author Dknx01 <e.witthauer@gmail.com>
 * @since 2017-08-29
 */

namespace Dknx01\ObjectXml;

class NamespaceData
{
    /**
     * @var string
     */
    private $namespace;

    /**
     * @var string
     */
    private $qualifiedName;

    /**
     * @param string $namespace
     *
     * @return $this
     */
    public function setNamespace($namespace)
    {
        $this->namespace = $namespace;

        return $this;
    }

    /**
     * @param string $qualifiedName
     *
     * @return $this
     */
    public function setQualifiedName($qualifiedName)
    {
        $this->qualifiedName = $qualifiedName;

        return $this;
    }

    /**
     * @return string
     */
    public function getNamespace()
    {
        return $this->namespace;
    }

    /**
     * @return string
     */
    public function getQualifiedName()
    {
        return $this->qualifiedName;
    }


}
