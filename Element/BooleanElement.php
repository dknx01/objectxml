<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 21:09
 */

namespace Dknx01\ObjectXml\Element;

use Dknx01\ObjectXml\Collection\RestrictionCollection;
use Dknx01\ObjectXml\Restriction\BooleanRestriction;

/**
 * An element that represents a boolean element. valid values are true, false, 0 and 1.
 */
class BooleanElement extends BaseElement
{
    /**
     * @inheritDoc
     */
    public function __construct($content, $tagName, $attributes = null, $restriction = null)
    {
        $restrictionCollection = new RestrictionCollection();
        $restrictionCollection->add(new BooleanRestriction());
        $restrictionCollection->add($restriction);

        parent::__construct($content, $tagName, $attributes, $restrictionCollection);
    }
}