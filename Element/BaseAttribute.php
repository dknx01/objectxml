<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 21:32
 */

namespace Dknx01\ObjectXml\Element;

use Dknx01\ObjectXml\Exception\InvalidNameException;
use Dknx01\ObjectXml\Restriction\RestrictionInterface;

/**
 * A basic attribute
 */
class BaseAttribute implements AttributeInterface, RestrictionAwareInterface
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var mixed
     */
    private $value;

    /**
     * @var RestrictionInterface
     */
    private $restriction;

    /**
     * BaseAttribute constructor.
     * @param string $name
     * @param mixed $value
     * @param RestrictionInterface $restriction
     */
    public function __construct($name, $value, RestrictionInterface $restriction = null)
    {
        $this->name = $name;
        $this->value = $value;
        $this->restriction = $restriction;
        $this->validateName();
        $this->validateRestriction();
    }

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @inheritdoc
     */
    public function getValue()
    {
        return$this->value;
    }

    /**
     * @throws InvalidNameException
     */
    private function validateName()
    {
        if (empty($this->name)) {
            throw new InvalidNameException('Attribute name cannot be empty');
        }
    }

    /**
     * @inheritDoc
     */
    public function validateRestriction()
    {
        if (!is_null($this->restriction)) {
            $this->restriction->validate($this->value);
        }
    }
}