<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 12.09.16 20:22
 */

namespace Dknx01\ObjectXml\Element;

use Dknx01\ObjectXml\Collection\AttributeCollection;

/**
 * Interface for all Elements that implementing attributes
 */
interface AttributesAwareInterface
{
    /**
     * add an attribute
     *
     * @param AttributeInterface $attribute
     *
     * @return $this
     */
    public function addAttribute(AttributeInterface $attribute);

    /**
     * returns all set attributes
     *
     * @return AttributeCollection
     */
    public function getAttributes();
}