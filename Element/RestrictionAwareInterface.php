<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 21:01
 */

namespace Dknx01\ObjectXml\Element;

/**
 * Interface for informing that the class have restrictions that should be validated
 */
interface RestrictionAwareInterface
{
    /**
     * the method to validate the set restrictions
     */
    public function validateRestriction();
}