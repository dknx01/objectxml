<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 21:10
 */

namespace Dknx01\ObjectXml\Element;

use Dknx01\ObjectXml\Collection\RestrictionCollection;
use Dknx01\ObjectXml\Restriction\DateTimeRestriction;

/**
 * Element for date_time value
 */
class DateTimeElement extends BaseElement
{
    /**
     * @inheritDoc
     */
    public function __construct($content, $tagName, $attributes = null, $restriction = null)
    {
        $restrictionCollection = $this->createRestrictionCollection();
        $restrictionCollection->add($restriction);
        parent::__construct($content, $tagName, $attributes, $restrictionCollection);
    }

    /**
     * @return RestrictionCollection
     */
    protected function createRestrictionCollection()
    {
        $restrictionCollection = new RestrictionCollection();
        $restrictionCollection->add(new DateTimeRestriction());
        return $restrictionCollection;
    }
}