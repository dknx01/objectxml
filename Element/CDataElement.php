<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 03.10.16 21:27
 */

namespace Dknx01\ObjectXml\Element;

use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Collection\RestrictionCollection;
use Dknx01\ObjectXml\Exception\InvalidAttributeException;
use Dknx01\ObjectXml\Exception\InvalidTagNameException;
use Dknx01\ObjectXml\Restriction\GenericStringRestriction;
use Dknx01\ObjectXml\Restriction\RestrictionInterface;

/**
 * @inheritdoc
 */
class CDataElement extends BaseElement implements CDataAwareInterface
{
    /**
     * @inheritDoc
     *
     * @throws InvalidAttributeException
     */
    public function __construct(
        $content,
        $tagName = null,
        AttributeCollection $attributes = null,
        RestrictionInterface $restriction = null
    ) {
        if (!is_null($attributes) && !$attributes->isEmpty()) {
            throw new InvalidAttributeException('A CDATA element must not have any attributes');
        }

        $restrictionCollection = $this->createRestrictionCollection();
        $restrictionCollection->add($restriction);

        parent::__construct($content, $tagName, $attributes, $restriction);
    }

    /**
     * @inheritDoc
     */
    protected function validateTagName()
    {
        if (!empty($this->tagName)) {
            throw new InvalidTagNameException('A CDATA element must not have a tag name');
        }
    }

    /**
     * @return RestrictionCollection
     */
    protected function createRestrictionCollection()
    {
        $restrictionCollection = new RestrictionCollection();
        $restrictionCollection->add(new GenericStringRestriction());
        return $restrictionCollection;
    }
}