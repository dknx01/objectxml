<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 21:33
 */

namespace Dknx01\ObjectXml\Element;

/**
 * Interface for all attributes
 */
interface AttributeInterface
{
    /**
     * returns the name of the attribute
     *
     * @return string
     */
    public function getName();

    /**
     * return the value of the attribute
     *
     * @return mixed
     */
    public function getValue();
}