<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 2017-08-31
 */

namespace Dknx01\ObjectXml\Element;

/**
 * An element that represents a boolean element, but the content values are 'true' or 'false.
 * valid values are true, false, 0 and 1.
 */
class BooleanAsStringElement extends BooleanElement
{
    /**
     * @inheritdoc
     */
    public function getContent()
    {
        return $this->content === true ? 'true' : 'false';
    }
}
