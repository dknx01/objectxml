<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 20:44
 */

namespace Dknx01\ObjectXml\Element;

/*
 * The interface for all elements
 */
interface ElementInterface
{
    /**
     * The value of the type
     *
     * @return mixed
     */
    public function getContent();

    /**
     * The xml tag name of the type
     *
     * @return string
     */
    public function getTagName();
}