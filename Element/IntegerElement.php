<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 21:09
 */

namespace Dknx01\ObjectXml\Element;

use Dknx01\ObjectXml\Collection\RestrictionCollection;
use Dknx01\ObjectXml\Restriction\IntegerRestriction;

/**
 * Element for integer values
 */
class IntegerElement extends FloatElement
{
    /**
     * @inheritdoc
     */
    protected function createRestrictionCollection()
    {
        $restrictionCollection = new RestrictionCollection();
        $restrictionCollection->add(new IntegerRestriction());
        return $restrictionCollection;
    }
}