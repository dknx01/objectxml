<?php

namespace Dknx01\ObjectXml\Element;

use Dknx01\ObjectXml\NamespaceData;

/**
 * @author Erik Witthauer <erik.witthauer@little-bird.de>
 * @since 2017-08-29
 * @copyright 2017 LITTLE BIRD GmbH
 */
interface NamespaceAwareInterface
{
    /**
     * @return NamespaceData
     */
    public function getNamespaceData();
}
