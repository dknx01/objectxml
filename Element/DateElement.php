<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 21:10
 */

namespace Dknx01\ObjectXml\Element;

use Dknx01\ObjectXml\Collection\RestrictionCollection;
use Dknx01\ObjectXml\Restriction\DateRestriction;

/**
 * Element for date value
 */
class DateElement extends DateTimeElement
{
    /**
     * @inheritDoc
     */
    protected function createRestrictionCollection()
    {
        $restrictionCollection = new RestrictionCollection();
        $restrictionCollection->add(new DateRestriction());
        return $restrictionCollection;
    }
}