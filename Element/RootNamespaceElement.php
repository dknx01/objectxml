<?php
/**
 * @author Dknx01 <e.witthauer@gmail.com>
 * @since 2017-08-30
 */

namespace Dknx01\ObjectXml\Element;

use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\NamespaceData;

class RootNamespaceElement implements NamespaceAwareInterface, AttributesAwareInterface
{
    /**
     * @var AttributeCollection
     */
    private $attributes;

    /**
     * @var NamespaceData
     */
    private $namespaceData;

    public function __construct(NamespaceData $namespaceData)
    {
        $this->attributes = new AttributeCollection();
        $this->namespaceData = $namespaceData;
    }

    /**
     * @inheritdoc
     */
    public function addAttribute(AttributeInterface $attribute)
    {
        $this->attributes->add($attribute);

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @inheritdoc
     */
    public function getNamespaceData()
    {
        return $this->namespaceData;
    }
}
