<?php
/**
 *
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 20:41
 */

namespace Dknx01\ObjectXml\Element;

use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Exception\InvalidTagNameException;
use Dknx01\ObjectXml\Restriction\RestrictionInterface;

/**
 * A basic element for xml files
 */
class BaseElement implements ElementInterface, AttributesAwareInterface, RestrictionAwareInterface
{
    /**
     * @var string
     */
    protected $tagName;

    /**
     * @var mixed
     */
    protected $content;

    /**
     * @var AttributeCollection
     */
    protected $attributes;

    /**
     * @var RestrictionInterface
     */
    protected $restriction;

    /**
     * @param mixed $content
     * @param string $tagName
     * @param AttributeCollection|null $attributes
     * @param RestrictionInterface|null $restriction
     */
    public function __construct(
        $content,
        $tagName,
        AttributeCollection $attributes = null,
        RestrictionInterface $restriction = null
    ) {
        $this->tagName = $tagName;
        $this->content = $content;
        $this->attributes = $attributes instanceof AttributeCollection ? $attributes : new AttributeCollection();
        $this->restriction = $restriction;
        $this->validateTagName();
        $this->validateRestriction();
    }

    /**
     * @inheritDoc
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @inheritDoc
     */
    public function getTagName()
    {
        return $this->tagName;
    }

    /**
     * @inheritDoc
     */
    public function addAttribute(AttributeInterface $attribute)
    {
        $this->attributes->set($attribute->getName(), $attribute->getValue());

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @inheritDoc
     */
    public function validateRestriction()
    {
        if (!is_null($this->restriction)) {
            $this->restriction->validate($this->content);
        }
    }

    /**
     * @throws InvalidTagNameException
     */
    protected function validateTagName()
    {
        if (empty($this->tagName)) {
            throw new InvalidTagNameException('Tag name cannot be empty');
        }
    }
}