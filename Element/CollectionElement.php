<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 15.09.16 21:09
 */

namespace Dknx01\ObjectXml\Element;

use Dknx01\ObjectXml\Collection\AttributeCollection;
use Dknx01\ObjectXml\Collection\CollectionInterface;

/**
 * An element that represents a collection of other element for xml elements with sub element.
 * Example:
 * <foo>
 *     <bar>value</bar>
 * </foo>
 *
 * new CollectionElement(new StringElement('value', 'bar'), 'foo');
 */
class CollectionElement extends BaseElement implements CollectionInterface
{
    /**
     * @inheritDoc
     */
    public function __construct($content, $tagName, $attributes = null, $restriction = null)
    {
        $this->content = array();
        if (is_array($content)) {
            foreach ($content as $item) {
                $this->add($item);
            }
        } else {
            $this->add($content);
        }
        $this->tagName = $tagName;
        $this->attributes = $attributes instanceof AttributeCollection ? $attributes : new AttributeCollection();
        $this->restriction = $restriction;
        $this->validateTagName();
        $this->validateRestriction();
    }

    /**
     * @inheritDoc
     * @param ElementInterface $element
     */
    public function add($element)
    {
        $this->content[$element->getTagName()] = $element;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function clear()
    {
        $this->content = array();
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function isEmpty()
    {
        return empty($this->content);
    }

    /**
     * @inheritDoc
     */
    public function remove($key)
    {
        if (array_key_exists($key, $this->content)) {
            unset($this->content[$key]);
        }
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function containsKey($key)
    {
        return array_key_exists($key, $this->content);
    }

    /**
     * @inheritDoc
     */
    public function get($key)
    {
        return $this->containsKey($key) ? $this->content[$key] : null;
    }

    /**
     * @inheritDoc
     */
    public function set($key, $value)
    {
        $this->content[$key] = $value;
        return $this;
    }

    /**
     * @inheritDoc
     */
    public function exists(\Closure $p)
    {
        foreach ($this->content as $key => $element) {
            if ($p($key, $element)) {
                return true;
            }
        }
        return false;
    }

    /**
     * @inheritDoc
     */
    public function filter(\Closure $p)
    {
        return $this->createFrom(array_filter($this->content, $p));
    }

    /**
     * @inheritDoc
     */
    public function map(\Closure $func)
    {
        return $this->createFrom(array_map($func, $this->content));
    }

    /**
     * @inheritDoc
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->content);
    }

    /**
     * @inheritDoc
     */
    public function count()
    {
        return count($this->content);
    }

    /**
     * Creates a new instance from the specified elements.
     *
     * This method is provided for derived classes to specify how a new
     * instance should be created when constructor semantics have changed.
     *
     * @param array $elements Elements.
     *
     * @return static
     */
    private function createFrom(array $elements)
    {
        array_walk($elements, function ($value, $key) use(&$elements) {
            if (!$value instanceof ElementInterface) {
                unset($elements[$key]);
            }
        });
        return new static($elements, $this->tagName, $this->attributes, $this->restriction);
    }
}