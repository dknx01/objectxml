<?php
/**
 * @author dknx01 <e.witthauer@gmail.com>
 * @since 11.09.16 19:56
 */

namespace Dknx01\ObjectXml;

use ArrayIterator;
use Dknx01\ObjectXml\Element\ElementInterface;
use Dknx01\ObjectXml\Exception\InvalidArgumentException;

/**
 * the xml object that holds all xml element and is used for generating the xml string
 */
class ObjectXml implements \Countable, \IteratorAggregate
{
    /**
     * @var array
     */
    private $elements = array();

    /**
     * @param $element
     * @return ObjectXml
     * @throws InvalidArgumentException
     */
    public function add($element)
    {
        if (!$element instanceof ElementInterface) {
            $actualType = is_object($element) ? get_class($element) : gettype($element);
            throw new InvalidArgumentException(
                'The element must be an instance of ElementInterface, ' . $actualType . ' given'
            );
        }
        $this->elements[] = $element;
        return $this;
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->elements);
    }

    /**
     * @return ArrayIterator
     */
    public function getIterator()
    {
        return new ArrayIterator($this->elements);
    }
}