# ObjectXml #
A PHP library to generate xml string/documents in an object oriented way including xsd validation and restriction for values

# Changelog #
* 0.7 adding CData element

# Creating a new object #

```
#!php

$xmlObj = new Dknx01\ObjectXml\ObjectXml();
```

# Restrictions #
Each element can have restrictions which are automatically executed for the given value.

**If you want to create your own restriction it must implement the Dknx01\ObjectXml\Restriction\RestrictionInterface**.

# Element #
Each xml element is represented by an own element object. There are some basic element:

| Element class | description |
|---|---|
| BaseElement | The base element |
| BooleanElement | Element for boolean values |
| CollectionElement | Collection of other element |
| DateElement | Element for date values |
| DateTimeElement | Element for datetime values |
| FloatElement | Element for float values |
| IntegerElement | Element for integer values |
| StringElement | Element for string values |
| TimeElement | Element for time values |

* **If you want to create your own element it must implement the Dknx01\ObjectXml\Element\ElementInterface.**
* **If you want to use attributes in must implement the Dknx01\ObjectXml\Element\AttributesAwareInterface.**
* **If you want to use restrictions it must implement the Dknx01\ObjectXml\Element\RestrictionAwareInterface.**

# Attributes #
Each element object can have one or more attributes. Each attribute can also have restrictions like a xml object.
If you have more than one attribute, you can use the attribute collection.

The BaseAttribute is the normal attribute with a name and a value (string) and no restriction.

**If you want to create your own attribute it must implement the Dknx01\ObjectXml\Element\AttributeInterface**

# Help #
For help have a look into Dknx01\ObjectXml\Resources\examples